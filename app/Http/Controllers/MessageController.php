<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use App\Events\MessageCreate;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::latest()->with('user')->take(20)->get();
        return response()->json($messages, 200);
    }

    public function store(Request $request)
    {
        $message = auth()->user()->messages()->create([
            'body' => $request->body
        ]);

        broadcast( new MessageCreate($message) )->toOthers();

        return response()->json($message, 200);
    }
}
