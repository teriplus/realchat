<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'body'
    ];

    protected $appends = [
        'amIOwner'
    ];

    public function getAmIOwnerAttribute()
    {
        return $this->user_id === auth()->user()->id ? true : false;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
