import Bus from './bus'

Echo.join('chat')
.here   ((users) => {
    Bus.$emit('users.here', users)
})
.joining((user) => {
    Bus.$emit('user.joining', user)
})
.leaving((user) => {
    Bus.$emit('user.leaving', user)
})
.listen('MessageCreate', (response) => {
    Bus.$emit('message.added', response.message)
})
